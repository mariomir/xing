//
//  AppDelegate.h
//  xing
//
//  Created by Mario Mir Cerrudo on 27/1/15.
//  Copyright (c) 2015 Mario Mir Cerrudo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

