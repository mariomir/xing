//
//  main.m
//  xing
//
//  Created by Mario Mir Cerrudo on 27/1/15.
//  Copyright (c) 2015 Mario Mir Cerrudo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
